pipeline {

    agent any

    tools {
      maven 'MAVEN3'
      jdk 'JDK11'
    }

    environment {
        NEXUS_VERSION = "nexus3"
         NEXUS_PROTOCOL = "http"
        NEXUS_URL = "localhost:8081"
        NEXUS_REPOSITORY = "maven-nexus-repo"
        NEXUS_CREDENTIAL_ID = "nexus-user-credentials"
        
    }

    stages {
        stage ('clone'){
            steps{
            git "https://gitlab.com/sandramekamgang/spring-petclinic-jenkins-pipeline.git"
            }
        }
        stage('build') {
           
                steps{
                sh "mvn clean install"
                } 
           
        }

        stage('execute Performance Tests') {
            steps{
            sh '/etc/apache-jmeter-5.4.1/bin/jmeter.sh -n -t /petclinic/src/test/jmeter/petclinic_test_plan.jmx -l test.jml'
            }
        
        }

        stage("publish to nexus") {
            steps {
                script {
                    pom = readMavenPom file: "pom.xml";
                    filesByGlob = findFiles(glob: "target/*.${pom.packaging}");
                    echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"
                    artifactPath = filesByGlob[0].path;
                    artifactExists = fileExists artifactPath;

                    if(artifactExists) {
                        echo "*** File: ${artifactPath}, group: ${pom.groupId}, packaging: ${pom.packaging}, version ${pom.version}";

                        nexusArtifactUploader(
                            nexusVersion: NEXUS_VERSION,
                            protocol: NEXUS_PROTOCOL,
                            nexusUrl: NEXUS_URL,
                            groupId: pom.groupId,
                            version: pom.version,
                            repository: NEXUS_REPOSITORY,
                            credentialsId: NEXUS_CREDENTIAL_ID,
                            artifacts: [
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: artifactPath,
                                type: pom.packaging],
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: "pom.xml",
                                type: "pom"]
                            ]
                        );

                    } else {
                        error "*** File: ${artifactPath}, could not be found";
                    }
                }
            }
        }
      stage('Building Image petclinic et mysql') {
      steps{
        sh 'docker build -t petclinic .'
        sh 'docker-compose up -d'
      }
    }
    stage('Push Petclinic mysql') {
            steps{
        withCredentials([azureServicePrincipal('azure-connexion')]){
        sh 'az login --service-principal -u $AZURE_CLIENT_ID -p $AZURE_CLIENT_SECRET -t $AZURE_TENANT_ID'
        sh 'az account set -s $AZURE_SUBSCRIPTION_ID'
        sh 'az acr login -n petRegistry'
        //sh 'docker build -t petclinic .'
        sh 'docker tag  petclinic petregistry.azurecr.io/petclinic:latest'
        sh 'docker tag  mysql:5.7 petregistry.azurecr.io/mysql:5.7'
        sh 'docker push petregistry.azurecr.io/petclinic:latest'
        sh 'docker push petregistry.azurecr.io/mysql:5.7'
                }
            }
        }
        stage('Select AKS cluster') {
            steps{  
        withCredentials([azureServicePrincipal('azure-connexion')]){
        sh 'az login --service-principal -u $AZURE_CLIENT_ID -p $AZURE_CLIENT_SECRET -t $AZURE_TENANT_ID'
        sh 'az account set -s $AZURE_SUBSCRIPTION_ID'
        sh 'az acr login -n petRegistry'
        //sh 'az account set --subscription 5788b126-1828-46f0-a5ed-a4eb6ff3b683'
        sh 'az aks get-credentials --resource-group petclinicRG --name petcluster'
        //sh 'az aks update -n petcluster -g petclinicRG --attach-acr petRegistry'

        sh 'kubectl apply -f deployement.yml'
        sh 'kubectl get deployments --all-namespaces=true'      
                }
            }
        }    


    }
}
